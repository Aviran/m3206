
====exercice 1================

1)
  
Sur la branche master

Validation initiale

Fichiers non suivis:
  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)

	./

aucune modification ajoutée à la validation mais des fichiers non suivis sont présents (utilisez "git add" pour les suivre)

2)

commande pour écrire la première ligne du fichier TD1_Git
echo "Première ligne du fichier" > TD1_Git.txt

3) résultat du git status



Sur la branche master

Validation initiale

Modifications qui seront validées :
  (utilisez "git rm --cached <fichier>..." pour désindexer)

	nouveau fichier : repTd1.md

Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git checkout -- <fichier>..." pour annuler les modifications dans la copie de travail)

	modifié :         repTd1.md

Fichiers non suivis:
  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)

	TD1_Git.txt

4) résultat du git status après avoir activer le suivi du fichier TD1_Git.txt






Sur la branche master

Validation initiale

Modifications qui seront validées :
  (utilisez "git rm --cached <fichier>..." pour désindexer)

	nouveau fichier : TD1_Git.txt
	nouveau fichier : repTd1.md

Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git checkout -- <fichier>..." pour annuler les modifications dans la copie de travail)

	modifié :         repTd1.md


5) résultat git status après le push 


Sur la branche master
Votre branche est basée sur 'origin/master', mais la branche amont a disparu.
  (utilisez "git branch --unset-upstream" pour corriger)
Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git checkout -- <fichier>..." pour annuler les modifications dans la copie de travail)

	modifié :         repTd1.md

aucune modification n'a été ajoutée à la validation (utilisez "git add" ou "git commit -a")

6) résultat du git push origin master


Sur la branche master
Votre branche est à jour avec 'origin/master'.
Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git checkout -- <fichier>..." pour annuler les modifications dans la copie de travail)

	modifié :         repTd1.md

aucune modification n'a été ajoutée à la validation (utilisez "git add" ou "git commit -a")
