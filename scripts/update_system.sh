#!/usr/bin/env bash

if [[ whoami -eq "root" ]]
then
	echo "[...]update database[...]"
	sudo apt-get -qq update 
	echo "[...]upgrade system[...]"
	sudo apt-get -q upgrade
else 
	echo "you're not root, try to run the script with 'sudo'"
fi


