#!/usr/bin/env bash

touch /tmp/netCheck
echo "[...] Checking internet connexion [...]"

{
ping -c 1 www.google.com 2> /tmp/netCheck
}&> /dev/null

if [[ $(cat /tmp/netCheck) != "" ]] 
then
	echo "[/!\] Not connected to Internet [/!\]"
	echo "[/!\] Please check configuration [/!\]"
	
	exit 5
else
	echo "[...] Connected to Internet [...]"
	exit 0
fi


